
#PATH=$PATH:$HOME/.rvm/bin # Add RVM to PATH for scripting
#export PATH=$HOME/local/bin:$PATH
# Run twolfson/sexy-bash-prompt
. ~/.bash_prompt

### Added by the Heroku Toolbelt
export PATH="/usr/local/heroku/bin:$PATH"

ssh-add ~/.ssh/scott-mac-mini-key

alias hal="fab -f ~/.fab/hal/fabfile.py"
