# Source z
. /usr/local/bin/z.sh

export PATH=$PATH:/Applications/Postgres.app/Contents/Versions/9.4/bin

alias ll="ls -lahG"

# RubyGems/Rails
alias rails="bundle exec rails"
alias rdm="rake db:migrate"
alias rs="bundle exec rails server"
alias rc="rails console"
alias sgem="sudo gem"

# Git
alias gc="git checkout"
alias gl="git log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr)%Creset' --abbrev-commit --date=relative"
alias gs="git status" 
alias gp="git push"

# Nano/Misc
alias sn="sudo nano"
alias flushcache="dscacheutil -flushcache"

# Development
alias sw="sass --watch"

# Docker
alias dm="docker-machine"

alias docker-agraph="docker run -d --net=host agraph && echo \"Running Allegrograph instance at http://$(docker-machine ip):10035\" && sleep 2s && open http://$(docker-machine ip):10035"

# HAL's fab alias
alias hal="fab -f ~/.fab/hal/fabfile.py"

# DigitalOcean fab alias
alias ocean="fab -f ~/.fab/digitalocean/fabfile.py"

# Node / NPM
export NODE_PATH=$NODE_PATH:/lib/node_modules

# Jupyter Notebook
alias book="jupyter notebook"

# Display current git branch in bash prompt
function parse_git_branch { 
   git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/' 
} 
export PS1="\$(parse_git_branch) \W \u $ "
#[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*

# Setting PATH for Python 2.7
# The orginal version is saved in .bash_profile.pysave
 PATH="/System/Library/Frameworks/Python.framework/Versions/2.7/bin:${PATH}"
 export PATH

# fucking dumb hack to fix python
# alias python="python2.7"

# Setting PATH for Python 2.7
# The orginal version is saved in .bash_profile.pysave
PATH="/Library/Frameworks/Python.framework/Versions/2.7/bin:${PATH}"
export PATH

# Fixing pip
alias pi="pip install -d /System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/site-packages/"

# fix mysql_config
export PATH="/usr/local/mysql/bin:${PATH}"

##
# Your previous /Users/scott/.bash_profile file was backed up as /Users/scott/.bash_profile.macports-saved_2014-10-12_at_16:10:48
##

# MacPorts Installer addition on 2014-10-12_at_16:10:48: adding an appropriate PATH variable for use with MacPorts.
export PATH="/opt/local/bin:/opt/local/sbin:$PATH"
# Finished adapting your PATH environment variable for use with MacPorts.

# Maven
export PATH=/usr/local/apache-maven-3.2.3/bin:$PATH

# Java
export JAVA_HOME=$(/usr/libexec/java_home)
# Trigger ~/.bashrc commands
. ~/.bashrc

##
# Your previous /Users/scott/.bash_profile file was backed up as /Users/scott/.bash_profile.macports-saved_2015-02-11_at_12:05:29
##

# MacPorts Installer addition on 2015-02-11_at_12:05:29: adding an appropriate PATH variable for use with MacPorts.
export PATH="/opt/local/bin:/opt/local/sbin:$PATH"
# Finished adapting your PATH environment variable for use with MacPorts.


##
# Your previous /Users/scott/.bash_profile file was backed up as /Users/scott/.bash_profile.macports-saved_2015-10-28_at_16:09:38
##

# MacPorts Installer addition on 2015-10-28_at_16:09:38: adding an appropriate PATH variable for use with MacPorts.
export PATH="/opt/local/bin:/opt/local/sbin:$PATH"
# Finished adapting your PATH environment variable for use with MacPorts.


##
# Your previous /Users/scott/.bash_profile file was backed up as /Users/scott/.bash_profile.macports-saved_2015-11-04_at_15:09:40
##

# MacPorts Installer addition on 2015-11-04_at_15:09:40: adding an appropriate PATH variable for use with MacPorts.
export PATH="/opt/local/bin:/opt/local/sbin:$PATH"
# Finished adapting your PATH environment variable for use with MacPorts.


# added by Anaconda2 2.4.0 installer
export PATH="/Users/scott/anaconda/bin:$PATH"

# Setting PATH for Python 3.5
# The orginal version is saved in .bash_profile.pysave
PATH="/Library/Frameworks/Python.framework/Versions/3.5/bin:${PATH}"
export PATH

#
# alias virtualenv3
alias virtualenv3='~/Library/Python/3.5/bin/virtualenv'
